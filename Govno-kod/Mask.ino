
#include "font.h"

#define DATA_PIN 2
#define SH_PIN 3
#define ST_PIN 4

const uint8_t crosses[10][3] = {
  0b00000000, 0b00000000, 0b00000000,
  0b00100000, 0b10000001, 0b00000100,
  0b00010001, 0b00000000, 0b10001000,
  0b00001010, 0b00000000, 0b01010000,
  0b00000100, 0b00000000, 0b00100000,
  0b00001010, 0b00000000, 0b01010000,
  0b00010001, 0b00000000, 0b10001000,
  0b00100000, 0b10000001, 0b00000100,
  0b00000000, 0b00000000, 0b00000000,
  0b00000000, 0b00000000, 0b00000000
};

const uint8_t lines[10][3] = {
  0b00000000, 0b00000000, 0b00000000, 
  0b00000100, 0b00000000, 0b00100000, 
  0b00001010, 0b00000000, 0b01010000, 
  0b00010001, 0b00000000, 0b10001000, 
  0b00100000, 0b10000001, 0b00000100, 
  0b00000000, 0b00000000, 0b00000000, 
  0b00000000, 0b00000000, 0b00000000, 
  0b00000000, 0b00000000, 0b00000000, 
  0b00000000, 0b00000000, 0b00000000, 
  0b00000000, 0b00000000, 0b00000000
};

const uint8_t circles[10][3] = {
  0b00000000, 0b00000000, 0b00000000, 
  0b00001110, 0b00000000, 0b01110000, 
  0b00010001, 0b00000000, 0b10001000, 
  0b00100000, 0b10000001, 0b00000100, 
  0b00100000, 0b10000001, 0b00000100, 
  0b00100000, 0b10000001, 0b00000100, 
  0b00010001, 0b00000000, 0b10001000, 
  0b00001110, 0b00000000, 0b01110000, 
  0b00000000, 0b00000000, 0b00000000, 
  0b00000000, 0b00000000, 0b00000000
};
const uint8_t hearts[10][3] = {
  0b00000000, 0b00000000, 0b00000000, 
  0b00000000, 0b00000000, 0b00000000, 
  0b00011011, 0b00000000, 0b11011000, 
  0b00100100, 0b10000001, 0b00100100, 
  0b00100000, 0b10000001, 0b00000100, 
  0b00010001, 0b00000000, 0b10001000, 
  0b00001010, 0b00000000, 0b01010000, 
  0b00000100, 0b00000000, 0b00100000, 
  0b00000000, 0b00000000, 0b00000000, 
  0b00000000, 0b00000000, 0b00000000
};
const uint8_t questions[10][3] = {
  0b00000000, 0b00000000, 0b00000000, 
  0b00001110, 0b00000000, 0b01110000, 
  0b00010001, 0b00000000, 0b10001000, 
  0b00010001, 0b00000000, 0b10001000, 
  0b00000010, 0b00000000, 0b00010000, 
  0b00000100, 0b00000000, 0b00100000, 
  0b00000100, 0b00000000, 0b00100000, 
  0b00000000, 0b00000000, 0b00000000, 
  0b00000100, 0b00000000, 0b00100000, 
  0b00000000, 0b00000000, 0b00000000
};

const uint8_t lantern[10][3] = { 
  255 , 255 , 255, 
  255 , 255 , 255, 
  255 , 255 , 255, 
  255 , 255 , 255,
  255 , 255 , 255,
  255 , 255 , 255,
  255 , 255 , 255,
  255 , 255 , 255,
  255 , 255 , 255,
  255 , 255 , 255
};

const uint8_t zeros[10][3] = { 
  0 , 0 , 0, 
  0 , 0 , 0, 
  0 , 0 , 0, 
  0 , 0 , 0,
  0 , 0 , 0,
  0 , 0 , 0,
  0 , 0 , 0,
  0 , 0 , 0,
  0 , 0 , 0,
  0 , 0 , 0
};

uint8_t buff[10][3] = {0};

int j = 0;
int k = 0;

void setup() {
    pinMode(DATA_PIN, OUTPUT);
    pinMode(SH_PIN, OUTPUT);
    pinMode(ST_PIN, OUTPUT);
    copy(buff,crosses);
    Serial.begin(9600);
}

int timing = 0;

int mode = 0;
String txt;

void loop() {
   if (Serial.available() > 0){
      mode = Serial.read(); 
      Serial.println(mode , DEC);
   }
   
   timing++;
   //copy(buff,crosses);
   
   switch (mode){
      case 48: text(); break;
      case 49: copy(buff,crosses); break;
      case 50: copy(buff,lantern); break;
      case 51: copy(buff,questions); break;
      case 52: pattern_1(); break;
      case 53: pattern_2(); break;
      default: pattern_1(); break;
   }
   draw();
}

void copy (uint8_t x[10][3] ,uint8_t y[10][3] ){
   for ( int i = 0 ; i < 10 ; i++)
      for (int j = 0 ; j < 3 ; j++)
        x[i][j] = y[i][j];
}

void draw(){
  for ( int i = 0 ; i < 10 ; i++ ){    
    digitalWrite(ST_PIN, LOW);

    shiftOut(DATA_PIN, SH_PIN, LSBFIRST, ~(0b00000001 <<(15-i)) );
    shiftOut(DATA_PIN, SH_PIN, MSBFIRST, ~(0b00000001 << i) );

    //uint8_t let = getFont('a',i);
    for (int j = 2 ; j >= 0  ; j--) {
      //shiftOut(data_pin, sh_pin, LSBFIRST, let );
    shiftOut(DATA_PIN, SH_PIN, LSBFIRST, buff[i][j]);
    }

    digitalWrite(ST_PIN, HIGH);
  }
}

void shift(){
  for (int i = 0 ; i < 10 ; i++){
       buff[i][0] = buff[i][0] << 1;
       buff[i][0] = buff[i][0] | ( buff[i][1] >> 7 );
       buff[i][1] = buff[i][1] << 1;
       buff[i][1] = buff[i][1] | ( buff[i][2] >> 7 );
       buff[i][2] = buff[i][2] << 1;
  }
}

uint8_t shift_flag = 1;
void pattern_1(){
   if (timing > 1100){
      copy(buff,circles);
      timing = 0;
      shift_flag = 1;
      return;
    }
  if (timing > 1050 && shift_flag){
    shift();
    shift_flag = 0;
    return;
  }
  if (timing > 400)
  {
  copy(buff,crosses);
  }
}

void pattern_2(){
  if (timing > 200){
    copy(buff,zeros);
    timing = 0;
    return;
  }

  if (timing > 100){
    copy(buff,questions);
  }
}

void text(){
    char text[] = " DC 20e6 ";
    //if (Serial.available() > 1){
    //txt = Serial.read();
    //Serial.println(txt);
    //}
    
    uint8_t let;

    if ( k == 7 ){
        if ( text[j+1] != 0 ){
            j++ ;
            k = 0;
        }
        else {
        j = 0 ;
        }
      } 

    if (timing > 6 ){   

      timing = 0;

      shift();
        for ( int i = 0 ; i < 8 ; i++ ){
          let = getFont(text[j],i);
          buff[i][2] |= 0b00000001 & (let >> k) ;
        }
      k++;
    }
}
